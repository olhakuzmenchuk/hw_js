import React from "react";
import "./HeaderRegistration.scss";

class HeaderRedistration extends React.Component {
  render() {
    return (
      <div className="header-redistration">
          <img
            className="header-redistration__icon"
            src="img/header-icon/Registrationicon.svg"
            alt="redistration login"
          ></img>
      </div>
    );
  }
}

export default HeaderRedistration;
