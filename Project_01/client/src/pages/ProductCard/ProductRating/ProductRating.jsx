import React from "react";

import {ReactComponent as RatingStarWhite} from './svg/rating-starW.svg'
import {ReactComponent as RatingStarColor} from './svg/rating-starO.svg'

const ProductRating = () => {
  return (
    <>
      <RatingStarColor/> <RatingStarColor/> <RatingStarColor/> <RatingStarColor/> <RatingStarWhite/>
    </>
  )
  
};
export default ProductRating;
