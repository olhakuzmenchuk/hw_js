// export const  selectModalRender = (state) => state.modalRender;
export const selectorNewCollectionProduct = (state) =>
  state.newCollectionProduct;
export const selectorCategoryEarrings = (state) => state.category.earrings;
export const selectBestsellers = (state) => state.bestsellers;

export const selectOutlet = (state) => state.outlet;