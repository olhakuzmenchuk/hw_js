import React, { Component } from 'react'

import "./modal.scss"

export default class Modal extends Component {
    render () {
        const {configModal, onClose} = this.props;
        const {text, header, closeButton, actions} = configModal;

        return(            
            <div className='modal-wrapper'onClick = {() => {onClose();}}>
            <div className='modal' id ="modal">
                <header className='modal-header'>{header}</header>
                <p className='content'>{text}</p>
                {!!closeButton && <span className = "close-btn" onClick = {() => {
                    onClose();
                }}></span>}
                <div className='button-wraper'>{actions}</div>
            </div>
            </div>
        )
            }
}