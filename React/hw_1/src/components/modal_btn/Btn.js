import React, { Component } from 'react'

export default class ModalBtn extends Component {
    
    render() {
       const {text, closeModal} = this.props;

        return(
            <button className="modal-button" type="button" onClick = {closeModal}> {text} </button>
        ) 
    }}