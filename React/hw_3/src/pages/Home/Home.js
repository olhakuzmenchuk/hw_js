import React, { useState, useEffect } from "react";
import Loader from "../../components/Loader/Loader";
import axios from "axios";
import CardsList from "../../components/CardsList/CardsList";

function Home(props) {
  const [goods, setGoods] = useState([null]);
  const [loding, setLoading] = useState(true);
  useEffect(() => {
    axios("/api/goods.json").then((res) => {
      const localArr = JSON.parse(localStorage.getItem("favorite"));
      if (localArr) {
        res.data.forEach((cardEl) => {
          localArr.find((e) => cardEl.id === e.id && (cardEl.favorite = true));
          setGoods(res.data);
        });
      } else {
        setGoods(res.data);
      }
      setLoading(false);
    });
  }, []);
  if (loding) {
    return <Loader />;
  }
  return <CardsList goods={goods} deleteProduct={false} />;
}

export default Home;
