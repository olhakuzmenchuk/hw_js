import React from "react";
import Button from "./Button";
import { render } from "@testing-library/react";

const testCard = {
  id: 1,
};

test("Smoke test of Button", () => {
  render(<Button />);
});

test("Button test with toggle function", () => {
  const toggleModal = jest.fn();
  render(<Button onClick={(testCard) => toggleModal(testCard)} />);
  expect(toggleModal).toBeDefined();
  expect(toggleModal).not.toHaveBeenCalled();
});

test("Button test for card", () => {
  render(<Button card={testCard} />);
  expect(testCard).not.toBeNull();
});

test("Button test for text content", () => {
  const { getByText } = render(<Button />);
  getByText(/add to cart/i);
});

test("Snapshot test for button", () => {
  const { container } = render(<Button />);
  expect(container.innerHTML).toMatchInlineSnapshot(
    `"<div><button class=\\"btn\\">Add to cart</button></div>"`
  );
});
