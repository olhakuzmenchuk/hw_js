const API_URL = 'https://ajax.test-danit.com/api/swapi/films '
const container = document.querySelector('.posts-list')


const sendRequest = async (url) => {
	const response = await fetch(url);
	const result = await response.json();
	return result;
}

const getPostFilms = (url) => {
	sendRequest(url)
	.then(data => { 
		console.log(data); 
		data.forEach(({episodeId, name, openingCrawl, characters }) => {
			container.insertAdjacentHTML('afterbegin', `<ul> <p class='episodeId'> ${episodeId} </p>
			<p class='name'> ${name} </p>
			<p class='list'></p>
			<p class='openingCrawl'> ${openingCrawl} </p></ul>`)
	
		getCharacters(characters);
		})
		.catch((error) => {
			console.log(error.messange);
		})
		
	})

} 

const getCharacters = (characters) => {
	const containerPerson = document.querySelector('.list')	
	const request = characters.map(url => fetch(url));
	Promise.all(request)
		.then(response => {
			const result = response.map(response => response.json());
			Promise.all(result).then(result => {
				result.forEach(({name}) => {
					const creatLi = document.createElement('li')
					creatLi.innerText = name;
					containerPerson.append(creatLi)

				})
			})
			.catch((error) => {
				console.log(error.messange);
			})
			
		})
}

getPostFilms(API_URL) 



