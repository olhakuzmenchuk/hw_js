
class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this._salary = salary;
    }
    set name(valueName){
        return this._name = valueName;
    }
    get name(){
        return this._name;
    }
    set age(valueAge){
        return this._age = valueAge;
    }

    get age(){
        return this._age;
    }
    set salary(valueSalary){
        return this._salary = valueSalary;
    }
    get salary(){
        return this._salary;
    }

}
const employee = new Employee('Anna', 30, 1000); 
console.log(employee);
console.log(employee.salary);


class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary); 
        this.lang = lang; 
    }

    set lang(valueLang){
        return this._lang = valueLang;
    }
    get lang(){
        return this._lang;
    }
    get salary(){
        return this._salary * 3; 
    }
}


const programmer = new Programmer('Olha', 45, 2000, 'eng')
console.log(programmer);
console.log(programmer.salary);

const programmerOne = new Programmer('Nastay', 18, 2500, 'eng', 'ru')
console.log(programmerOne);
console.log(programmerOne.salary);


