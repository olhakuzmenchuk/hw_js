const API = "https://api.ipify.org/?format=json";
const serverAPI = "http://ip-api.com/json/";
const filter = "?fields=continent,country,regionName,city,district";
const btn = document.querySelector('.btn');
const posts = document.querySelector('.posts');

const sendRequest = async (url) => {
	const response = await fetch(url);
	const result = await response.json();
	return result;
}

btn.addEventListener('click', async()=>{
  posts.innerText = "";
  posts.insertAdjacentHTML('afterbegin',`
  <div class="titel">
  <h2 class="title">Получиная информация</h2>
  <p class="loader"></p>
  </div>
  `);
  const titelCard = posts.querySelector('.titel');
  try {
    const Obj = await sendRequest(API);
    const data = await sendRequest(`${serverAPI}${Obj.ip}${filter}`);
    titelCard.lastElementChild.remove();
    const {continent,country,regionName,city} = data;
    titelCard.insertAdjacentHTML('beforeend',`
          <ul class = "titel">
            <li>Континент</li>
            <li>Страна</li>
            <li>Регион</li>
            <li>Город</li>
          </ul>
          <ul class = "info">
            <li>${continent}</li>
            <li>${country}</li>
            <li>${regionName}</li>
            <li>${city}</li>
          </ul>
    `);
  } catch (error) {
    titelCard.lastElementChild.remove();
    console.log(error);
  }
})
