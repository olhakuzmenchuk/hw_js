/**
 * # Завдання
 * Розробити функціонал для редагування наявних постів
 *
 * ## Технічні вимоги
 * * АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 * * при зміні значення в селекті отримуємо з сервера наявний пост по ID
 * * отримані дані (title та body) відображаємо в полях вводу
 * * при натисканні на кнопку Оновлення відправляти PATCH запит по вибраному ID поста
 *
 * ## Advanced
 * * при завантаженні сторінки робити запит на весь список постів та відображати існуючі варіанти в селекті
 *
 * */

//1 найти все элементы формы
//2 написать функцию для запроса (обработка data)
//3 при при выборе номера поста из селекта нужно выполнить запрос GET и получить тайтл и дескрипшин поста(отоброзить его в инпутах)
//4 при submit нам нужно выполнить запрос PUT и отправить на сервер изененный пост

const API = 'https://ajax.test-danit.com/api/json/';

const form = document.querySelector('#post-form');
const [formTitle, formBody] = document.querySelectorAll('[type="text"]');
const selectPost = document.querySelector('#posts');
const selectUser = document.querySelector('#users');
const postsDiv = document.querySelector('#postsWrapper');

const getUsers = () => {
    fetch(`${API}users/`)
    .then(response => response.json())
    .then(users => {
        users.forEach(({id}) => {
            selectUser.insertAdjacentHTML('beforeend', `
                <option value="${id}">${id}</option>
            `)
        });
    })
}
getUsers()

const getPosts = (id) => {
    fetch(`${API}users/${id}/posts`)
    .then(response => response.json())
    .then(posts => {
        selectPost.innerHTML = '<option value="none" disabled selected>ID поста</option>'

        posts.forEach(({id}) => {
            selectPost.insertAdjacentHTML('beforeend', `
                <option value="${id}">${id}</option>
            `)
        });
    })
}

const getPost = (postId) => {
    fetch(`${API}posts/${postId}`)
    .then(response => response.json())
    .then(({title, body}) => { // деструктуризация запроса GET получение самого поста по ID
        formTitle.value=title;
        formBody.value=body;
    })
}

const updatePost = (postId, requestBody) => {
    fetch(`${API}posts/${postId}`, {
        method:"PUT",// метод обновления данных по индификатору postId
        body: JSON.stringify(requestBody) ///"{"title":"sddssd","body":"sdfdfdsfd"'}" + {"id": id}
    })
        .then(response => response.json())
        .then((post) => {
            console.log(post);
        })
        .catch((error)=> {
            alert(error.message)
        })
}

selectUser.addEventListener('change',(event)=>{
    const userId = event.target.value;
    postsDiv.style.display = 'block' 
    getPosts(userId)
})

selectPost.addEventListener('change', (event)=>{
    const postId = event.target.value;
    getPost(postId);
})

form.addEventListener('submit', (event)=>{
    event.preventDefault();
    const postId = selectPost.value;

    if(formTitle.value && formBody.value) {
        const post = {
            title: formTitle.value,
            body: formBody.value,
        }
        updatePost(postId, post);
    } else {
        alert('Инпуты не могут быть пустыми!')
    }
})