import React from 'react';

import logo from './logo.svg';
import logoRedux from './logo_redux.svg';

import './App.css';

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <div className="container">
                    <div className="App-header-wrapper">
                        <img src={logo} className="App-logo" alt="logo"/>
                        <img src={logoRedux} className="App-logo" alt="logo"/>
                    </div>
                </div>
            </header>
            <main className="container">
            </main>
        </div>
    );
}

export default App;
