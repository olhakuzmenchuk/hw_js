
// В файле index.html лежит разметка для кнопок.
// Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана эта буква, 
// должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена 
//в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет.
// Далее, пользователь нажимает S, 
// и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черно



const buttons = document.querySelectorAll('.btn'); //содержащий все найденные элементы документа в div btn-wrapper

const pressedLetter = (elem) => {
	console.log(elem.key, elem.key.toLowerCase());
	buttons.forEach((btn) => {
		btn.classList.remove('btn-pressed');
		if (elem.key.toLowerCase() === btn.dataset.key) {
			btn.classList.add('btn-pressed');
		}
	});
};

window.addEventListener('keydown', pressedLetter);



//elem.classList – это специальный объект с методами для добавления/удаления одного класса.
//обратиться к таким атрибутам как с свойствам объекта, то это делается не на прямую, а при помощи специального свойства dataset:
//window.addEventListener добавляем события в окно браузер
//keydown Событие keydown срабатывает, когда клавиша была нажата.