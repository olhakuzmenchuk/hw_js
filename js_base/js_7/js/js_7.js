// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

// Технические требования:
// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;
// Примеры массивов, которые можно выводить на экран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можно взять любой другой массив.


function createList(arr, parent) { 
    if(!parent){ 
        parent = document.body; 
    } 
    const listUl = document.createElement('ul') 
    const listLi = arr.map(elem => `<li>${elem}</li>`) 
    listUl.insertAdjacentHTML("beforeend", listLi.join('')) 
    parent.append(listUl); 
 
 
    // setTimeout(()=> listUl.remove(),3000) 
} 
 
 
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);












