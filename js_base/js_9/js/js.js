
// В папке tabs лежит разметка для вкладок. Нужно, 
// чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
//  При этом остальной текст должен быть скрыт. В комментариях указано,
//   какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться,
//  и что вкладки могут добавляться и удаляться. При этом нужно, 
//  чтобы функция, написанная в джаваскрипте, 
//  из-за таких правок не переставала работать.

const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.content');

tabs.addEventListener('click', (elem) => {
	const tabsTitle = tabs.querySelectorAll('.tabs-title');

	tabsTitle.forEach((tabTitle) => {
		tabTitle.classList.remove('active');
	});

	elem.target.classList.add('active');
	let tabAttr = elem.target.getAttribute('data-content');

    showContent(tabAttr);
});

const showContent = ((attr) => {
    tabsContent.forEach(tabContent => {
        if(tabContent.getAttribute('data-content') === attr){
            tabContent.classList.remove('content');
        } else {
            tabContent.classList.add('content');
        }
    });
});

//метод querySelector() возвращает первый элемент (Element) документа, который соответствует указанному селектору или группе селекторов.
// Метод querySelectorAll() Document возвращает статический (не динамический) NodeList, содержащий все найденные элементы документа, которые соответствуют указанному селектору. 
//Свойство target интерфейса Event является ссылкой на объект, который был инициатором события.
// getAttribute() возвращает значение указанного атрибута элемента. Если элемент не содержит данный атрибут, могут быть возвращены null или "" (пустая строка);



// Считать с помощью модального окна браузера число, которое введет пользователь.
// С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
// Использовать синтаксис ES6 для работы с перемеными и функциями.

// После ввода данных добавить проверку их корректности. Если пользователь не ввел число, либо при вводе указал не число,
//  - спросить число заново (при этом значением по умолчанию для него должна быть введенная ранее информация).

let a;
let rusalt = 1;

do {
     a = prompt(`enter number`)
}while(!a || isNaN(+a) || a.trim() === ``)

const factorial = function(a){
   if(a > 1){
    rusalt *= a;
    factorial(a - 1);
   } 
 return rusalt;
}
console.log(factorial(a));


// let rusalt = 1;

// for (let i = 1; i <= a; i++){
//    rusalt = rusalt * i;

// }
// console.log(rusalt)

// for (; a >= 1; a--) {
//     rusalt = rusalt * a;
// }
// console.log(rusalt)
